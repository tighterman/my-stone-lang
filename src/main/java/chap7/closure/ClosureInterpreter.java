package chap7.closure;

import chap6.BasicInterpreter;
import chap7.NestedEnv;
import stone.ClosureParser;
import stone.ParseException;

public class ClosureInterpreter extends BasicInterpreter {

    public static void main(String[] args) throws ParseException {
        run(new ClosureParser(), new NestedEnv());
    }

}
