package chap6;

import javassist.gluonj.*;
import stone.StoneException;
import stone.Token;
import stone.ast.*;

import java.util.List;

@Reviser
public class BasicEvaluator {

    public static final int TRUE = 1;
    public static final int FALSE = 0;

    @Reviser
    public static abstract class ASTreeEx extends ASTree {
        public abstract Object eval(Environment environment);
    }

    @Reviser
    public static class ASTListEx extends ASTList {

        public ASTListEx(List<ASTree> children) {
            super(children);
        }

        public Object eval(Environment environment) {
            throw new StoneException("cannot eval: " + toString(), this);
        }
    }

    @Reviser
    public static class ASTLeafEx extends ASTLeaf {

        public ASTLeafEx(Token t) {
            super(t);
        }

        public Object eval(Environment environment) {
            throw new StoneException("cannot eval: " + toString(), this);
        }
    }

    @Reviser
    public static class NumberEx extends NumberLiteral {

        public NumberEx(Token t) {
            super(t);
        }

        public Object eval(Environment environment) {
            return value();
        }

    }

    @Reviser
    public static class StringEx extends StringLiteral {

        public StringEx(Token t) {
            super(t);
        }

        public Object eval(Environment environment) {
            return value();
        }

    }

    @Reviser
    public static class NameEx extends Name {

        public NameEx(Token t) {
            super(t);
        }

        public Object eval(Environment environment) {
            Object value = environment.get(name());
            if (value == null) {
                throw new StoneException("undefined name: " + name(), this);
            } else {
                return value;
            }
        }

    }

    @Reviser
    public static class NegativeEx extends NegativeExpr {

        public NegativeEx(List<ASTree> children) {
            super(children);
        }

        public Object eval(Environment environment) {
            Object v = ((ASTreeEx)oprand()).eval(environment);
            if (v instanceof Integer) {
                return -(Integer) v;
            } else {
                throw new StoneException("bad type for -", this);
            }
        }

    }

    @Reviser
    public static class BinaryEx extends BinaryExpr {

        public BinaryEx(List<ASTree> children) {
            super(children);
        }

        public Object eval(Environment environment) {
            String op = operator();
            if ("=".equals(op)) {
                Object right = ((ASTreeEx)right()).eval(environment);
                return computeAssign(environment, right);
            } else {
                Object left = ((ASTreeEx)left()).eval(environment);
                Object right = ((ASTreeEx)right()).eval(environment);
                return computeOp(left, op, right);
            }
        }

        protected Object computeOp(Object left, String op, Object right) {
            if (left instanceof Integer && right instanceof Integer) {
                return computeNumber((Integer)left, op, (Integer)right);
            } else if (op.equals("+")) {
                return String.valueOf(left) + String.valueOf(right);
            } else if (op.equals("==")) {
                if (left == null) {
                    return right == null ? TRUE : FALSE;
                } else {
                    return left.equals(right) ? TRUE : FALSE;
                }
            } else {
                    throw new StoneException("bad type", this);
            }
        }

        protected Object computeNumber(Integer left, String op, Integer right) {
            int a = left.intValue();
            int b = right.intValue();
            switch (op) {
                case "+":
                    return a + b;
                case "-":
                    return a - b;
                case "*":
                    return a * b;
                case "/":
                    return a / b;
                case "%":
                    return a % b;
                case "==":
                    return a == b ? TRUE : FALSE;
                case "<":
                    return a < b ? TRUE : FALSE;
                case ">":
                    return a > b ? TRUE : FALSE;
                default:
                    throw new StoneException("bad operator", this);
            }
        }

        protected Object computeAssign(Environment environment, Object right) {
            ASTree l = left();
            if (l instanceof Name) {
                environment.put(((Name) l).name(), right);
                return right;
            } else {
                throw new StoneException("bad assignment", this);
            }
        }

    }

    @Reviser
    public static class BlockEx extends BlockStmnt {

        public BlockEx(List<ASTree> children) {
            super(children);
        }

        public Object eval(Environment environment) {
            Object result = 0;
            for (ASTree t : this.children) {
                if (!(t instanceof NullStmnt)) {
                    result = ((ASTreeEx)t).eval(environment);
                }
            }
            return result;
        }

    }

    @Reviser
    public static class IfEx extends IfStmnt {

        public IfEx(List<ASTree> children) {
            super(children);
        }

        public Object eval(Environment environment) {
            Object c = ((ASTreeEx)condition()).eval(environment);
            if (c instanceof Integer && ((Integer)c).intValue() != FALSE) {
                return ((ASTreeEx)thenBlock()).eval(environment);
            } else {
                ASTree b = elseBlock();
                if (b == null) {
                    return 0;
                } else {
                    return ((ASTreeEx)elseBlock()).eval(environment);
                }
            }
        }

    }

    @Reviser
    public static class WhileEx extends WhileStmnt {

        public WhileEx(List<ASTree> children) {
            super(children);
        }

        public Object eval(Environment environment) {
            Object result = 0;
            for (;;) {
                Object c = ((ASTreeEx)condition()).eval(environment);
                if (c instanceof Integer && ((Integer)c).intValue() == FALSE) {
                    return result;
                } else {
                    result = ((ASTreeEx)body()).eval(environment);
                }
            }
        }

    }

}
