package chap9;

import chap6.BasicEvaluator;
import chap6.Environment;
import chap7.FuncEvaluator;
import chap7.NestedEnv;
import javassist.gluonj.Require;
import javassist.gluonj.Reviser;
import stone.StoneException;
import stone.ast.*;

import java.util.List;

@Require(FuncEvaluator.class)
@Reviser
public class ClassEvaluator {

    @Reviser
    public static class ClassStmntEx extends ClassStmnt {

        public ClassStmntEx(List<ASTree> children) {
            super(children);
        }

        public Object eval(Environment env) {
            ClassInfo ci = new ClassInfo(this, env);
            env.put(name(), ci);
            return name();
        }

    }

    @Reviser
    public static class ClassBodyEx extends ClassBody {

        public ClassBodyEx(List<ASTree> children) {
            super(children);
        }

        public Object eval(Environment env) {
            for (ASTree t : this) {
                ((BasicEvaluator.ASTreeEx)t).eval(env);
            }
            return null;
        }

    }

    @Reviser
    public static class DotEx extends Dot {

        public DotEx(List<ASTree> children) {
            super(children);
        }

        public Object eval(Environment env, Object value) {
            String member = name();
            if (value instanceof ClassInfo) {
                if ("new".equals(member)) {
                    ClassInfo ci = (ClassInfo) value;
                    NestedEnv e = new NestedEnv(ci.environment());
                    StoneObject so = new StoneObject(e);
                    e.putNew("this", so);
                    initObject(ci, e);
                    return so;
                }
            } else if (value instanceof StoneObject) {
                try {
                    return ((StoneObject)value).read(member);
                } catch (StoneObject.AccessException ignored) {}
            }
            throw new StoneException("bad member access: " + member, this);
        }

        protected void initObject(ClassInfo ci, Environment env) {
            if (ci.superClass() != null) {
                initObject(ci.superClass(), env);
            }
            ((ClassBodyEx)ci.body()).eval(env);
        }

    }

    @Reviser
    public static class AssignEx extends BasicEvaluator.BinaryEx {

        public AssignEx(List<ASTree> children) {
            super(children);
        }

        @Override
        protected Object computeAssign(Environment environment, Object right) {
            ASTree l = left();
            if (l instanceof PrimaryExpr) {
                FuncEvaluator.PrimaryEx p = (FuncEvaluator.PrimaryEx) l;
                if (p.hasPostfix(0) && p.postfix(0) instanceof Dot) {
                    Object t = ((FuncEvaluator.PrimaryEx)l).evalSubExpr(environment, 1);
                    if (t instanceof StoneObject) {
                        return setField((StoneObject) t, (Dot) p.postfix(0), right);
                    }
                }
            }
            return super.computeAssign(environment, right);
        }

        protected Object setField(StoneObject obj, Dot expr, Object rvalue) {
            String name = expr.name();
            try {
                obj.write(name, rvalue);
                return rvalue;
            } catch (StoneObject.AccessException e) {
                throw new StoneException("bad member access: " + location() + ": " + name);
            }
        }

    }

}
