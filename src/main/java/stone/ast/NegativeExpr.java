package stone.ast;

import java.util.List;

public class NegativeExpr extends ASTList {

    public NegativeExpr(List<ASTree> children) {
        super(children);
    }

    public ASTree oprand() { // 看起来好像应该是child(1)
        return child(0);
    }

    @Override
    public String toString() {
        return "-" + oprand();
    }
}
