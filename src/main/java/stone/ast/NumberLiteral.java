package stone.ast;

import stone.Token;

public class NumberLiteral extends ASTLeaf {

    public NumberLiteral(Token t) {
        super(t);
    }

    public int value() {
        return token().getNumber();
    }

    @Override
    public String toString() {
        return Integer.toString(value());
    }
}
